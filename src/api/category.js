import Sequelize from 'sequelize';
import { Router } from 'express';
import Promise from 'core-js/es6/promise'

const Op = Sequelize.Op;

export default ({ config, db }) => {
  const Category = db.define('category',
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      parent: {
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      hasproducts: {
        type: Sequelize.BOOLEAN
      }
    },
    {
      timestamps: false
    });

  Category.sync({ force: false });

  let api = Router();


  api.get('/getChildren/:id?', (req, res) => {
    Category.findAll({
      where: {
        parent: {
          [Op.eq]: req.params.id || null
        }
      }
    }).then(result => {
      const items = result.map(item => {
        return {
          id: item.id,
          name: item.name,
          hasProducts: item.hasproducts
        };
      });

      res.json(result);
    });
  });


  api.get('/:id', (req, res) => {
    Category.findOne({
      where: {
        id: {
          [Op.eq]: req.params.id || null
        }
      }
    }).then(result => res.json(result));
  });

  const getParent = (arr, id) => {
    return Category.findOne({
      where: {
        id: {
          [Op.eq]: id
        }
      }
    }).then(category => {
      arr.unshift(category);

      if (category.parent) {
        return getParent(arr, category.parent);
      }

      return Promise.resolve(arr);
    });
  };

  api.get('/getPath/:id', (req, res) => {
    getParent([], req.params.id).then(result => {
      const items = result.map(item => {
        return {
          id: item.id,
          name: item.name
        };
      });

      res.json(items);
    });
  });

  return api;
}
