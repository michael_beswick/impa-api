import Sequelize from 'sequelize';

export default callback => {
	const sequelize = new Sequelize('database', 'username', 'password', {
		host: 'localhost',
		dialect: 'sqlite',
		operatorsAliases: false,

		pool: {
			max: 5,
			min: 0,
			acquire: 30000,
			idle: 10000
		},
		storage: 'data/impa-cat-compare.db'
	});

	callback(sequelize);
}
