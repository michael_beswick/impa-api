import { version } from '../../package.json';
import { Router } from 'express';
import category from './category';
import product from './product';

export default ({ config, db }) => {
	let api = Router();

	api.use('/category', category({ config, db }));
	api.use('/product', product({ config, db }));

	// // perhaps expose some API metadata at the root
	api.get('/', (req, res) => {
		res.json({ version });
	});

	return api;
}
