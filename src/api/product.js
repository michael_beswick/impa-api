import Sequelize from 'sequelize';
import { Router } from 'express';
import fs from 'fs';

const Op = Sequelize.Op;

export default ({ config, db }) => {
  const Product = db.define('product',
    {
      partno: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      description: {
        type: Sequelize.STRING
      },
      uom: {
        type: Sequelize.STRING
      },
      mtmluom: {
        type: Sequelize.STRING
      },
      category_id: {
        type: Sequelize.INTEGER
      }
    },
    {
      timestamps: false,
      indexes: [
        {
          unique: false,
          fields: ['category_id']
        },
        {
          unique: false,
          fields: ['description']
        },
      ]
    });

  Product.sync({ force: false });

  let api = Router();

  api.get('/getByCategoryId/:id', (req, res) => {
    Product.findAll({
      where: {
        category_id: {
          [Op.eq]: req.params.id || null
        }
      },
      limit: req.query.limit || 10,
      offset: req.query.offset || 0
    }).then(result => res.json(result));
  });

  api.get('/search', (req, res) => {
    const { query, categoryId } = req.query;
    const whereObj = {};

    if (query) {
      whereObj['description'] = {
        [Op.like]: query,
        [Op.like]: query.replace(' ', '%') + '%'
      };
    }

    if (categoryId) {
      whereObj['category_id'] = {
        [Op.eq]: categoryId
      }
    }

    Product.findAll({
      where: whereObj,
      limit: req.query.limit || 10,
      offset: req.query.offset || 0
    }).then(result => res.json(result));
  });

  api.get('/:partNo', (req, res) => {
    Product.findOne({
      where: {
        partno: {
          [Op.eq]: req.params.partNo || null
        }
      }
    }).then(result => res.json(result));
  });

  api.get('/image/:partNo', (req, res) => {
    const { partNo } = req.params;
    const dir = partNo.substring(0, 2);

    fs.readFile(`images/${dir}/${partNo}.jpg`, function (err, data) {
      if (err) throw err;

      res.writeHead(200, { 'Content-Type': 'image/jpeg' });
      res.end(data);
    });
  });


  return api;
}
